Package: fmask
Title: Automated cloud, cloud shadow and snow masking for Landsat TM/ETM+ images
Description: R implementation of Fmask (Function of mask), a tool for automatic
    cloud and cloud shadow detection in Landsat imagery developed by Zhe Zhu
    <zhuzhe@bu.edu> and Curtis E. Woodcock <curtis@bu.edu> at the Center for
    Remote Sensing, Department of Geography and Environment,Boston University.
Version: 0.1.5.1
Authors@R: c(person("Guillaume", "Drolet", role = c("aut", "cre"),
    email = "droletguillaume@gmail.com"),
    person("Joseph", "Henry", role = "ctb",
    email = "joseph.henry@sydney.edu.au"),
    person("Willem", "Vervoort", role = "ctb",
    email = "willem.vervoort@sydney.edu.au"),
    person("Dennis", "Duro", role = "ctb",
    email = "dennis.duro@glel.carleton.ca"),
    person("Mark", "Adams", role = "ctb",
    email = "mark.adams@sydney.edu.au"),
    person("John", "Sulik", role = "ctb",
    email = "john.sulik@ars.usda.gov"),
    person("Etienne", "Racine", role = "ctb",
    email = "etiennebr@gmail.com")
    )
Maintainer: Guillaume Drolet <droletguillaume@gmail.com>
Depends:
    R (>= 3.2.2),
    rgdal (>= 0.9-3),
    raster (>= 2.3-40),
    data.table (>= 1.9.4)
Imports:
    RSAGA (>= 0.94-1),
    Rcpp (>= 0.12.2),
    parallel (>= 3.3.1)
Suggests:
    igraph (>= 0.7.1),
    testthat (>= 0.10.0)
LinkingTo: Rcpp
License: GPL (>= 2)
LazyData: true
URL: https://bitbucket.org/droletg/fmask
BugReports: https://bitbucket.org/droletg/fmask/issues
RoxygenNote: 5.0.1
