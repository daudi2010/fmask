## fmask 0.1.5
* fixed a bug with metadata filename which has different tag names in different    
  versions of MTL files.

## fmask 0.1.4
* replaced `JAGUAR::RowMin()` with C++ function RowMin.cpp (from Chaitanya 
  Acharya, Duke University) and introduced use of Rcpp. This fixes a bug
  with package JAGUAR 3.0 which does not include function RowMin from earlier
  versions of the package.

## fmask 0.1.3
* fixed a bug with JAGUAR, which had been removed from DESCRIPTION.

## fmask 0.1.2
* added use of `JAGUAR::RowMin()` for speeding up finding of non-missing
  pixels.

## fmask 0.1.1
* Landsat 8 images, as well as Landsat 4, 5, and 7 that have the new MTL file 
  format can now be processed. However, execution speed for complete images
  is still VERY slow. Improving speed will be the next focus of future versions.
  
## fmask 0.1
* first working version of the package.
  