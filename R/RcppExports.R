# Generated by using Rcpp::compileAttributes() -> do not edit by hand
# Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#' Find the minimum value for each row of a matrix
#' 
#' @param x A matrix containing values of type numeric
#' @export
#' @author Chaitanya Acharya	(Duke University)
RowMin <- function(x) {
    .Call('fmask_RowMin', PACKAGE = 'fmask', x)
}

