#' Similarity 
#'
#' Calculates similarity between projected shadows and potential 
#' cloud and cloud shadow layers in a Landsat scene.
#' 
#' @param x Data table containing columns:
#' \itemize{
#'   \item{labels: }{original cloud objects IDs}
#'   \item{o.cell: }{original cloud objects cell numbers}
#'   \item{s.cell: }{projected shadow cell numbers}
#' }
#' @param y Data table containing a single column:
#' \itemize{
#'   \item{cell: }{cell numbers of cells in the combined potential cloud layer-
#'   cloud shadows out of image cells  (i.e. no-data values surrounding the 
#'   image)}
#' }
#' @return Data table of similarity values for each projected shadow object in
#' the image 
#' @details For each shadow object, the similarity value is the ratio of the 
#' overlap area between the calculated shadow and the potential cloud or shadow 
#' layers to the calculated shadow area, and is calculated as:
#' 
#' s = a / b, where:
#' 
#' a = number of overlapping pixels between b and combined po-
#'     tential cloud and potential cloud shadow layers plus those out-
#'     side of the image boundary
#' b = number of shadow pixels not in its corresponding cloud object 
#'     plus those outside of the image boundary
#'     
#' @keywords internal  
#' @author Guillaume Drolet
calculateSimilarity <- function(x, y) {
  
  # Within each shadow object:
  
  # - find the number of cells that fall outside of the image            
  outpix <- x[, list(out.ncell = sum(is.na(s.cell))), by = labels]
  
  # - find cells (sh.cells) and number of cells by cloud (sh.ncell) that do not 
  #   overlap with cells in its associated cloud object (projected cells that 
  #   fall outside of the image are exluded from similarity calculations)                  
  shdws <- x[!is.na(s.cell) & !(s.cell %in% o.cell), 
    .(sh.cell = s.cell, sh.ncell = length(s.cell)), by = labels]
  
  # - Within shdws, find the number of cells that overlap with 
  #   cells in the combined potential cloud or cloud shadow layers                     
  shdws[, isin.y := FALSE]    
  setkey(shdws, sh.cell)  
  
  shdws[y, isin.y := TRUE] 
  
  setkey(shdws, labels)
  shdws[outpix, c("sh.sum", "ov.sum") := 
      .(sh.ncell + out.ncell, sum(isin.y) + out.ncell), by = .EACHI]
  
  shdws[, match := round(ov.sum / sh.sum, digits = 2)]
  shdws[, c("sh.cell", "sh.ncell", "isin.y", "ov.sum", "sh.sum") := NULL]
  
  return(shdws)            
}
