#' Fmask algorithm constants
#'  
#' Returns a set of constants required by the Fmask algorithm
#'  
#' @param TSIMILAR Numeric. Minimum threshold that determines how much a 
#' projected shadows overlaps with the cloud and cloud shadow layers
#' @param TBUFFER Numeric. Maximum threshold for matching projected shadows with
#' the cloud and cloud shadow layers
#' @param RECORDTHRESH Numeric. Record threshold for matching projected shadows 
#' with the cloud and cloud shadow layers
#' @param NCLOUD.OBJ Integer. Minimum number of adjacent pixels necessary to 
#' identify a group of pixels as a cloud object
#' @param ENV.LAPSERATE Numeric. Lapse rate of wet air (degrees Kelvin/km)
#' @param DRY.LAPSERATE Numeric. Lapse rate of dry air (degrees Kelvin/km)
#' @param MAX.CLOUDH Numeric. Maximum cloud base height (m)
#' @param MIN.CLOUDH Numeric. Minimum cloud base height (m)
#' @param NPIX Integer. Number of inward pixels (240 m) for cloud base 
#' temperature
#' @param H Numeric. Average Landsat 4, 5, & 7 platform height (m)
#' @return A list containing parameter/value pairs of constants used by the 
#' Fmask algorithm
#' @note Calling constants() with no arguments returns a list with the default
#' constant values that yielded the best results in sensitivity analyses
#' (results can be found here: \url{http://dx.doi.org/10.1016/j.rse.2011.10.028}. 
#' Users of the fmask() function can modify these constants and pass the modified
#' list as an argument when calling fmask()
#' @export
#' @author Guillaume Drolet
#  version 1.0
constants <- function(TSIMILAR = 0.3, TBUFFER = 0.98, RECORDTHRESH = 0.95, 
                      NCLOUD.OBJ = 9, ENV.LAPSERATE = 6.5, DRY.LAPSERATE = 9.8, MAX.CLOUDH = 12000,   
                      MIN.CLOUDH = 200, NPIX = 8,  H = 705000) {
  l <- list(
    TSIMILAR = TSIMILAR, 
    TBUFFER = TBUFFER, 
    RECORDTHRESH = RECORDTHRESH,
    NCLOUD.OBJ = NCLOUD.OBJ, 
    ENV.LAPSERATE = ENV.LAPSERATE, 
    DRY.LAPSERATE = DRY.LAPSERATE, 
    MIN.CLOUDH = MIN.CLOUDH,
    MAX.CLOUDH = MAX.CLOUDH,
    NPIX = NPIX,
    H = H 
  )
  
  return(l)
}