#' Dilate
#'
#' Dilates pixels in a raster of 0s and 1s
#' 
#' @param x RasterLayer with only zeroes (0) and ones (1)
#' @param n Numeric. The size of the neighborhood to use for the dilation. For
#' example, if a 3-by-3 pixels neighborhood is needed, n = 3
#' @param t Numeric. Threshold to use for the dilation. For example, if t equals 
#' 5, pixels having 5 or more pixels equal to 1 in their neighborhood will have
#' the value 1
#' @return x: dilated version of RasterLayer x
#' @seealso \code{\link{focal}}
#' @keywords internal
#' @author Guillaume Drolet
#  version 1.0
dilate <- function(x, n, t) {
  
  p <- n^2
  w <- matrix(rep(1, p), nrow = n, ncol = n)
  w[ceiling(p/2)] <- 0  
  f <- focal(x, w = w)     
  x[f >= t] <- 1         
  
  return(x)
}
