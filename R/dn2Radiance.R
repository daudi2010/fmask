#' Image DN counts to radiance
#' 
#' Converts image digital counts (DN) to top-of-atmosphere (TOA) radiances.
#' 
#' @param dn RasterStack of DN bands, from subsetBandsEMS()
#' @param bp A list of band calibration parameters, from bandsParams()  
#' @param cl Cluster object with the required libraries and options loaded
#' @return RasterStack of bands converted to radiance [W/m2/sr/micron]
#' @seealso \code{\link{bandsParams}}, \code{\link{subsetBandsEMS}}, 
#' \code{\link[snow]{makeSOCKcluster}}, \code{\link[snow]{clusterEvalQ}}
#' @keywords internal
#' @author Guillaume Drolet
#  version 1.0  
dn2Radiance <- function(dn, bp, cl) {
  
  cal <- function(x) {
    
    a <- (bp[[x]]$LMAX - bp[[x]]$LMIN) / (bp[[x]]$QCALMAX - bp[[x]]$QCALMIN)
    b <- dn[[x]] - bp[[x]]$QCALMIN
    c <-  bp[[x]]$LMIN  
    a * b + c
  }
  
  if (!missing(cl)) { 
    
    parallel::clusterExport(cl, c("dn", "bp"), envir = environment())        
    rd <- parallel::parLapply(cl, names(dn), cal)    
  } else {
    rd <- lapply(names(dn), cal)
  }
  names(rd) <- names(dn)
  
  return(stack(rd))
}