#' Find non-missing pixels
#' 
#' Finds non-missing pixels in a RasterStack of Landsat bands DN counts.
#' 
#' Non-missing pixels are those not equal to raster::NAvalue(x), therefore
#' raster::NAvalue(x) must have been set correctly prior to calling this
#' function.
#' 
#' @param x RasterStack objects of Landsat bands (digital numbers).
#' @param cl Cluster object (optional), created using functions from the  
#'   \code{parallel} or \code{snow}. If supplied, the function will run in 
#'   parallel.
#' @param filename Character (optional). Filename of the output raster of 
#'   missing value, if needed. Default is an empty string "", in which case no 
#'   output raster is written to file. 
#' @return RasterLayer with non-missing pixels set to TRUE and missing pixels
#'   to FALSE.
#' @importFrom parallel clusterApplyLB clusterExport
#' @import raster
#' @keywords internal
#' @author Guillaume Drolet
findNonMissingPixels <- function(x, cl, filename = "") {
  
  stopifnot(class(x) == "RasterStack")
  
  band_names <- c("B", "G", "R", "NIR", "SWIR1", "TIR", "SWIR2")
  stopifnot(all(names(x) %in% band_names))
  
  nm <- raster::setValues(raster(x, layer = 0), TRUE)
  filename <- trim(filename)
  
  if (raster::canProcessInMemory(x, n = raster::nlayers(x))) {          
    nm[is.na(raster::getValues(x))] <- FALSE
  } else {
    bs <- raster::blockSize(x)
    
    get_missvals <- function(i) {     
      xs = seq_len(ncol(x))
      ys = seq(bs$row[i], length.out = bs$nrows[i])        
      cells <- raster::cellFromRowColCombine(x, ys, xs)
      
      missing <- data.table(celln = cells)    
      missing[, names(x) := lapply(names(x), function(s) {
        raster::getValues(x[[s]], bs$row[i], bs$nrows[i])
      })]     
      missing[, missing := .(apply(.SD, 1, function(x) any(is.na(x)))), 
        .SDcols = band_names]
      
      return(missing[missing == TRUE, celln]) 
    } 
    
    if (!missing(cl)) {
      stopifnot(class(cl)[2] == "cluster")
      
      parallel::clusterExport(cl, list("x", "bs", "band_names"), 
        envir = environment())    
      misscells <- unlist(parallel::clusterApplyLB(cl, seq_len(bs$n), 
        get_missvals))
    } else {
      misscells <- unlist(sapply(seq_len(bs$n), get_missvals))
    }
    nm[misscells] <- FALSE  
  } 
  names(nm) <- "nm"
  if (filename != "") nm <- raster::writeRaster(nm, filename, datatype = "LOG1S")
  
  return(nm)
}