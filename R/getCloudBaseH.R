#' Range of cloud base height
#' 
#' Calculates the range of cloud base heights in the image.
#' 
#' @param x RasterLayer of cloud base scaled brightness temperatures
#' @param th Numeric. t.tempH, returned by cloudProbLand()
#' @param tl Numeric. t.tempL, returned by cloudProbLand()
#' @param const List of constants object as returned by function constants()
#' @return RasterStack of 2 rasters of minimum and maximum cloud base heights 
#' @references Eq. 21 in \url{http://dx.doi.org/10.1016/j.rse.2011.10.028}
#' @seealso \code{\link{constants}}
#' @keywords internal
#' @author Guillaume Drolet
#  version 1.0
getCloudBaseH <- function(x, th, tl, const = constants()) {
  
  min.r <- x
  min.r[!is.na(min.r)] <- const$MIN.CLOUDH          
  min.cloudbaseh <- max(min.r, 10 * (tl - x) / const$DRY.LAPSERATE)    
  names(min.cloudbaseh) <- "min.cloudbase.height"
  
  max.r <- x
  max.r[!is.na(max.r)] <- const$MAX.CLOUDH          
  max.cloudbaseh <- min(max.r, 10 * (th - x))
  names(max.cloudbaseh) <- "max.cloudbase.height"
  
  h <- stack(min.cloudbaseh, max.cloudbaseh)
  
  return(h)
}
