#' Corrected cloud object pixel coordinates
#' 
#' Calculates new pixel coordinates corrected for satellite view geometry 
#' effects.
#' 
#' @param x Integer. Vector of cloud pixel x coordinates (columns)
#' @param y Integer. Vector of cloud pixel y coordinates (rows)
#' @param h Numeric. Vector of pixel cloud heights (m)
#' @param z Data frame containing view geometry parameters as returned by
#' viewGeo()
#' @param const List of constants as returned by constants()
#' @return pc Data frame of projected cloud object pixels coordinates (row, col)  
#' @references \url{http://code.google.com/p/fmask/}
#' @seealso \code{\link{constants}}
#' @keywords internal
#' @author Guillaume Drolet
#  version 1.0
matTrueCloud <- function(x, y, h, z, const = constants()) {
  
  # Distance from the central perpendicular (unit: pixel)
  dist = (z$A * x + z$B * y + z$C) / sqrt(z$A^2 + z$B^2) 
  
  dist.par = dist / cos(z$omega.per - z$omega.par)
  dist.move = dist.par * h / const$H # cloud move distance (m)
  
  delta.x = dist.move * cos(z$omega.par)
  delta.y = dist.move * sin(z$omega.par)        
  x = x + delta.x # columns
  y = y + delta.y # rows
  pc <- as.data.frame(cbind(x, y))
  
  return(pc)
}