#' Cloudy pixels
#' 
#' Returns a raster of cloud pixels.
#' 
#' @param bt RasterLayer of calibrated brightness temperature 
#' @param p RasterStack including these RasterLayers:
#' \itemize{
#'   \item{pcp: }{Potential cloud pixels}
#'   \item{water.test: }{Water Test}
#' }
#' @param w RasterLayer of cloud probability over water (wCloud.prob) from 
#' cloudProbWater()
#' @param l A list, as returned by cloudProbLand(), and containing these 
#' objects:
#' \itemize{
#'   \item{lCloud.prob: }{RasterLayer of cloud probabilities over land}
#'   \item{land.threshold: }{Numeric}
#'   \item{t.low: }{Numeric}
#' }
#' @return RasterLayer of cloud pixels, dilated using a 3-by-3 neighborhood      
#' @references Equation 18 in \url{http://dx.doi.org/10.1016/j.rse.2011.10.028}
#' @keywords internal
#' @author Guillaume Drolet    
#  version 1.0
potentialCloudLayer <- function(bt, p, w, l) {
  
  wclr.max <- 50 # Fixed threshold over water
  
  # Eq. 18 - Potential Cloud Layer (PCL)    
  pcl <- (p$pcp & p$water.test & w$wCloud.prob > wclr.max) | # cloud over water
    (p$pcp & !p$water.test & l$lCloud.prob > l$land.threshold) | # cloud over land
    (l$lCloud.prob > 99 & !p$water.test) | # high probability of cloud over land
    (bt < (l$t.low - 3500)) # really cold cloud  
  
  # Pixels having 5 or more cloudy pixels in a 3-by-3 neighborhood are set as 
  # cloudy pixels      
  pcl <- dilate(pcl, 3, 5)
  names(pcl) <- "potential.cloud.layer"
  
  return(pcl)  
}