% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/findSaturatedPixels.R
\name{findSaturatedPixels}
\alias{findSaturatedPixels}
\title{Find saturated pixels}
\usage{
findSaturatedPixels(x, s, cl, filename = "", ...)
}
\arguments{
\item{x}{RasterStack of DN bands}

\item{s}{Interger. Saturated pixel value}

\item{cl}{Cluster object with the required libraries and options loaded 
(optional)}
}
\value{
RasterLayer in which saturated pixels are equal to TRUE, others 
to FALSE
}
\description{
Finds saturated pixels in a RasterStack of Landsat bands DN counts.
}
\author{
Guillaume Drolet
}
\keyword{internal}

