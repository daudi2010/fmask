% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/scaleReflectiveBands.R
\name{scaleReflectiveBands}
\alias{scaleReflectiveBands}
\title{Scale reflective bands radiance/temperature}
\usage{
scaleReflectiveBands(rf, cl)
}
\arguments{
\item{rf}{RasterStack of reflective/temperature bands}

\item{cl}{Cluster object with the required libraries and options loaded
(optional)}
}
\value{
RasterStack of reflective bands scaled using a factor of 10^4
for bands in the visible to SWIR regions, and a factor 10^2 for band(s) in 
the thermal region ("TIR", in degrees Celcius)
}
\description{
Scales image reflective bands before running the Fmask algorithm 
(\code{\link{fmask}}).
}
\note{
Image bands (in physical units) MUST be scaled using this function 
prior to calling the fmask function.
}
\author{
Guillaume Drolet
}
\references{
\url{http://code.google.com/p/fmask/}
}
\keyword{internal}

